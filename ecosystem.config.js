module.exports = {
  apps : [
    {
      name: "billnet-cms-dev",
      script: "npm",
      args: "run dev"
    },
    {
      name: "billnet-cms-prod",
      script: "npm",
      args: "run start"
    },
    {
      name: "billnet-cms-staging",
      script: "npm",
      args: "run start"
    }
  ]
}
